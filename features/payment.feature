Feature: Payment

    Scenario: User created successfully
        Given user "Marie", "Seindal", 150286-9986, with balance 500 kr
        Then "Marie" exists
        And "Marie" exists in the bank
        And "Marie" has 500 kr

    Scenario: Payment successfully proceeded
        Given user "Marie", "Seindal", 150286-9986, with balance 500 kr
        And user "Jonas", "Sørensen", 150286-9987, with balance 500 kr
        When "Marie" pays "Jonas" 100 kr 
        Then "Jonas" has 600 kr 
        And "Marie" has 400 kr

    Scenario: Payment where debtor does not have required balance
        Given user "Marie", "Seindal", 150286-9986, with balance 50 kr
        And user "Jonas", "Sørensen", 150286-9987, with balance 500 kr
        When "Marie" fails to pay "Jonas" 100 kr
        Then "Marie" has 50 kr
        And "Jonas" has 500 kr

    Scenario: Payment where debtor is also the creditor
        Given user "Marie", "Seindal", 150286-9986, with balance 500 kr
        When "Marie" pays "Marie" 100 kr
        Then "Marie" has 500 kr

    Scenario: Payment where payment is negative
        Given user "Marie", "Seindal", 150286-9986, with balance 50 kr
        And user "Jonas", "Sørensen", 150286-9987, with balance 500 kr
        When "Marie" fails to pay "Jonas" -100 kr
        Then "Marie" has 50 kr
        And "Jonas" has 500 kr