package dk.group12;

import static org.junit.Assert.*;
import io.cucumber.java.en.*;
import io.cucumber.java.*;
import java.util.*;
import java.math.BigDecimal;

import dtu.ws.fastmoney.*;

public class PaymentSteps {
    
    // final PaymentService paymentService = new PaymentService();
    final Map<String, String> temp_users = new HashMap<>(); //make sure to make test users with unique first names


    // @After
    // public void remove_all_created_ids(Scenario scenario){
    //     for (String id : temp_users.values()) {
    //         try {
    //             paymentService.bankService.retireAccount(id);
    //         } catch (Exception e) {
    //             //scenario.write("Failed to delete id: " + id);
    //         }
    //     }
    // }

    // @ParameterType("\\d{6}-\\d{4}")
    // public String cpr(String string){
    //     return string;
    // }

    // @Given("user {string}, {string}, {cpr}, with balance {bigdecimal} kr")
    // public void user_with_balance_kr_is_created(String firstName, String lastName, String cpr, BigDecimal balance) throws Exception{
    //     final User new_user = new User();
    //     new_user.setFirstName(firstName);
    //     new_user.setLastName(lastName);
    //     new_user.setCprNumber(cpr);

    //     final String id = paymentService.bankService
    //         .createAccountWithBalance(
    //             new_user, 
    //             balance
    //         );

    //     temp_users.put(firstName, id);
    // }

    // @When("{string} pays {string} {bigdecimal} kr")
    // public void pays_kr(String debtor, String creditor, BigDecimal amount) throws Exception {
    //     final String debtor_id = temp_users.get(debtor);
    //     assertNotNull(debtor_id);
    //     final String creditor_id = temp_users.get(creditor);
    //     assertNotNull(creditor_id);
    //     final Payment payment = new Payment(debtor_id, creditor_id, amount, "test payment");
    //     paymentService.pay(payment);
    // }

    // @When("{string} fails to pay {string} {bigdecimal} kr") // Passes if payment fails
    // public void failure_pay_kr(String debtor, String creditor, BigDecimal amount) throws Exception {

    //     final String debtor_id = temp_users.get(debtor);
    //     assertNotNull(debtor_id);
    //     final String creditor_id = temp_users.get(creditor);
    //     assertNotNull(creditor_id);
    //     final Payment payment = new Payment(debtor_id, creditor_id, amount, "test failed payment");

    //     try {
    //         paymentService.pay(payment);
    //     } catch(Exception e) {
    //         return;            
    //     }
    //     throw new Exception("Payment should not succeed");
    // }

    // @Then("{string} exists")
    // public void exists(String firstName) throws Exception {
    //     final String id = temp_users.get(firstName);
    //     assertNotNull(id);
    // }

    // @Then("{string} exists in the bank")
    // public void exists_in_the_bank(String firstName) throws Exception {
    //     final String id = temp_users.get(firstName);
    //     assertNotNull(id);
    //     final Account account = paymentService.bankService.getAccount(id);
    //     assertNotNull(account);
    // }
    
    // @Then("{string} has {bigdecimal} kr")
    // public void has_kr(String firstName, BigDecimal balance) throws Exception {
    //     final String id = temp_users.get(firstName);
    //     assertNotNull(id);
    //     final Account account = paymentService.bankService.getAccount(id);
    //     assertNotNull(account);

    //     assertEquals(balance, account.getBalance());
    // }

}
