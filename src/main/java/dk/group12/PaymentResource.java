package dk.group12;

import java.util.Map;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Message;
import org.eclipse.microprofile.reactive.messaging.Metadata;
import org.eclipse.microprofile.reactive.messaging.Outgoing;
import org.jboss.logging.Logger;

import com.google.gson.Gson;

import dk.group12.events.CorrelationId;
import dk.group12.events.Event;
import io.smallrye.mutiny.Uni;
import io.smallrye.mutiny.tuples.Tuple2;
import io.vertx.core.json.JsonObject;
import io.smallrye.reactive.messaging.rabbitmq.*;
import io.smallrye.reactive.messaging.rabbitmq.OutgoingRabbitMQMetadata.Builder;
import java.util.*;

@ApplicationScoped
public class PaymentResource {

  private static final Logger LOG = Logger.getLogger(PaymentResource.class);

  @Inject
  PaymentService pService;

  /**
   * @author Chris
   * @param jObj
   * @return
   */
  @Incoming("request-payment")
  @Outgoing("payment-completed")
  public Uni<JsonObject> requestPayment(JsonObject jObj) {
    return pService.paymentRequest(jObj);
  }

  /**
   * @author Jonas
   * @param obj
   * @return
   */
  @Incoming("request-report")
  @Outgoing("report-completed")
  public Uni<Message<JsonObject>> requestReport(Message<JsonObject> obj) {
    Optional<IncomingRabbitMQMetadata> inMeta = obj.getMetadata(IncomingRabbitMQMetadata.class);
    Event ev = convertJsonToObject(obj.getPayload(), Event.class);
    Builder meta = OutgoingRabbitMQMetadata.builder();
    List<List<String>> report = new ArrayList<>();
    CorrelationId corrId = CorrelationId.randomId();

    final var header = inMeta.flatMap(IncomingRabbitMQMetadata::getReplyTo);

    if (header.isPresent()) {

      final var key = header.get();

      switch (key) {
        case "customer": {
          meta.withRoutingKey("customer");
          UUID userUUID = ev.getArgument(0, UUID.class);

          corrId = ev.getArgument(1, CorrelationId.class);

          report = pService.getCustomerReport(userUUID);

          LOG.info("Routing to customer");
          break;
        }

        case "merchant": {
          meta.withRoutingKey("merchant");
          UUID userUUID = ev.getArgument(0, UUID.class);

          corrId = ev.getArgument(1, CorrelationId.class);

          report = pService.getMerchantReport(userUUID);

          LOG.info("Routing to merchant");
          break;
        }

        case "manager": {
          meta.withRoutingKey("manager");

          corrId = ev.getArgument(0, CorrelationId.class);

          report = pService.getManagerReport();

          LOG.info("Routing to manager");
          break;
        }
      }
    }

    final var finalCorrId = corrId;

    OutgoingRabbitMQMetadata finalMeta = meta.build();
    return Uni.createFrom().item(report).log()
        .map(r -> new Event("account-registered", new Object[] { r, finalCorrId })).log()
        .map(this::convertObjectToJson)
        .map(jObj -> Message.of(jObj, Metadata.of(finalMeta)).withAck(obj.getAck()));

  }

  private <T> T convertJsonToObject(JsonObject jObj, Class<T> classType) {
    return new Gson().fromJson(jObj.toString(), classType);
  }

  private JsonObject convertObjectToJson(Object obj) {
    return new JsonObject(new Gson().toJson(obj, obj.getClass()));
  }

}
