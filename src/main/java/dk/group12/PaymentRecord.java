package dk.group12;

import lombok.*;
import java.util.*;

import java.math.BigDecimal;

@AllArgsConstructor
public class PaymentRecord{
    public final UUID debtorUUID, creditorUUID;
    public final String debtorToken;
    public final BigDecimal amount;
    public final String description;
}