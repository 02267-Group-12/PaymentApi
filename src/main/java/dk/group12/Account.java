package dk.group12;

import java.io.Serializable;
import java.util.UUID;

import lombok.Data;

@Data
public class Account implements Serializable {

  public Account(UUID account_uuid, String accountId, String accountType, String bankAccountNumber) {
    this.accountUUID = account_uuid;
    this.accountId = accountId;
    this.accountType = accountType;
    this.bankAccountId = bankAccountNumber;
  }
  private static final long serialVersionUID = -7578399191567331939L;
  private UUID accountUUID;
  private String accountId;
  private String accountType;
  private String bankAccountId;

}
