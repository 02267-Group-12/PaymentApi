package dk.group12;

import lombok.*;
import java.util.*;

import java.math.BigDecimal;

@Data
public class PaymentRequest{
    public UUID 
        debtorToken, // The payment-token/customer-token 
        creditorId; 
    public BigDecimal amount;
    public String description;
}