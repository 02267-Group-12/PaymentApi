package dk.group12;

import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.eclipse.microprofile.reactive.messaging.Incoming;
import org.eclipse.microprofile.reactive.messaging.Message;
import org.eclipse.microprofile.reactive.messaging.Metadata;
import org.jboss.logging.Logger;

import com.google.gson.Gson;

import dk.group12.events.CorrelationId;
import dk.group12.events.Event;

import java.io.UncheckedIOException;
import java.time.Duration;
import java.util.*;
import dtu.ws.fastmoney.*;
import io.smallrye.common.annotation.Blocking;
import io.smallrye.mutiny.Multi;
import io.smallrye.mutiny.Uni;
import io.smallrye.mutiny.tuples.Tuple;
import io.smallrye.mutiny.tuples.Tuple2;
import io.smallrye.mutiny.unchecked.Unchecked;
import io.smallrye.reactive.messaging.rabbitmq.OutgoingRabbitMQMetadata;
import io.vertx.core.json.JsonObject;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import io.smallrye.reactive.messaging.rabbitmq.*;
import io.smallrye.reactive.messaging.rabbitmq.OutgoingRabbitMQMetadata.Builder;

@ApplicationScoped
public class PaymentService {

    private static final Logger LOG = Logger.getLogger(PaymentService.class);

    // @Inject
    public final BankService bankService = new BankServiceService().getBankServicePort();

    // public final List<PaymentRecord> ledger = new ArrayList<>();

    // public final MessageQueue mq = new RabbitMqQueue();

    // public PaymentService() {
    // mq.addHandler("paymentRequest", this::paymentRequest);
    // mq.addHandler("tokenToUserIDCompleted", this::tokenToUserIDCompleted);
    // mq.addHandler("userIDtoBankIDCompleted", this::userIDtoBankIDCompleted);
    // }

    List<PaymentRecord> payments = new CopyOnWriteArrayList<>();

    @Inject
    @Channel("request-retrieve-account")
    Emitter<JsonObject> requestRetrieveAccount;

    @Inject
    @Channel("use-token")
    Emitter<JsonObject> useTokenQueue;

    Map<UUID, Uni<Account>> retrievedAccountMap = new ConcurrentHashMap<>();
    Map<UUID, Uni<UUID>> retrievedAccountUUIDMap = new ConcurrentHashMap<>();

    /**
     * @author Chris
     * @param jObj
     */
    @Incoming("account-retrieved")
    public void accountRetrieved(JsonObject jObj) {
        LOG.infof("Received event: %s", jObj);
        LOG.debugf("registeredAccountap: %s", retrievedAccountMap.toString());
        Event ev = convertJsonToObject(jObj, Event.class);
        Account account = ev.getArgument(0, Account.class);
        CorrelationId corrId = ev.getArgument(1, CorrelationId.class);
        retrievedAccountMap.computeIfPresent(corrId.getId(), (key, val) -> {
            LOG.infof("Setting the value Acc to: %s for corrId %s", account, corrId.getId());
            return val.onItem().transformToUni(c -> Uni.createFrom().item(account));
        });
    }

    /**
     * @author Chris
     * @param jObj
     */
    @Incoming("accountUUID-retrieved")
    public void accountUUIDRetrieved(JsonObject jObj) {
        LOG.infof("Received event: %s", jObj);
        LOG.debugf("registeredAccountUUIDMap: %s", retrievedAccountUUIDMap.toString());
        Event ev = convertJsonToObject(jObj, Event.class);
        UUID accountUUID = ev.getArgument(0, UUID.class);
        CorrelationId corrId = ev.getArgument(1, CorrelationId.class);
        retrievedAccountUUIDMap.computeIfPresent(corrId.getId(), (key, val) -> {
            LOG.infof("Setting the value AccUUID to: %s for corrId %s", accountUUID, corrId.getId());
            return val.onItem().transformToUni(c -> Uni.createFrom().item(accountUUID));
        });
    }

    /**
     * @author Chris
     * @param jObj
     * @return
     */
    public Uni<JsonObject> paymentRequest(JsonObject jObj) {
        LOG.info(jObj);
        Event ev = convertJsonToObject(jObj, Event.class);

        OutgoingRabbitMQMetadata meta = OutgoingRabbitMQMetadata.builder()
                .withReplyTo("payment").build();

        PaymentRequest pReq = ev.getArgument(0, PaymentRequest.class);
        CorrelationId corrId = ev.getArgument(1, CorrelationId.class);
        LOG.info(pReq);
        // LOG.info(corrId);

        // Request UUID from Token
        CorrelationId tokenCorrelationId = CorrelationId.randomId();
        CorrelationId debtorCorrelationId = CorrelationId.randomId();
        Event tEv = new Event("request-retrieve-account",
                new Object[] { pReq.getDebtorToken(), tokenCorrelationId });
        LOG.infof("tokenCorrelationId: \t\t %s", tokenCorrelationId);
        retrievedAccountUUIDMap.put(tokenCorrelationId.getId(), Uni.createFrom().nullItem());

        JsonObject tJsonObj = new JsonObject(new Gson().toJson(tEv));
        LOG.info("Sending token to accountUUID request");
        useTokenQueue.send(tJsonObj);

        Uni<UUID> tokenRequest = Uni.createFrom().item(retrievedAccountUUIDMap.get(tokenCorrelationId.getId()))
                .onItem().transformToUni(accountUUID -> retrievedAccountUUIDMap.get(tokenCorrelationId.getId()))
                .onItem().invoke(c -> LOG.infof("token retrieved acc of:%s", c))
                // Check for failure
                .onItem().ifNull().fail()
                // Deal with Failure
                .onFailure().retry().withBackOff(Duration.ofMillis(300), Duration.ofSeconds(10)).atMost(5)
                .onItem().call(() -> retrievedAccountUUIDMap.remove(tokenCorrelationId.getId()));

        // 
        tokenRequest.onItem().invoke(uuid -> {
            Event dEv = new Event("request-retrieve-account",
                    new Object[] { uuid, debtorCorrelationId });
            LOG.infof("debtorCorrelationId: \t\t %s", debtorCorrelationId);
            retrievedAccountMap.put(debtorCorrelationId.getId(), Uni.createFrom().nullItem());

            JsonObject dJsonObj = new JsonObject(new Gson().toJson(dEv));
            LOG.info("Sending debtor request");
            requestRetrieveAccount.send(Message.of(dJsonObj, Metadata.of(meta)));
        }).map(uuid -> debtorCorrelationId).log();

        // Request Debtor Account from UUID
        // CorrelationId debtorCorrelationId = CorrelationId.randomId();
        // Event dEv = new Event("request-retrieve-account",
        // new Object[] {pReq.debtorToken, debtorCorrelationId});
        // LOG.infof("debtorCorrelationId: \t\t %s", debtorCorrelationId);
        // retrievedAccountMap.put(debtorCorrelationId.getId(),
        // Uni.createFrom().nullItem());

        // Request Creditor Account from UUID
        CorrelationId creditorCorrelationId = CorrelationId.randomId();
        Event cEv = new Event("request-retrieve-account",
                new Object[] { pReq.creditorId, creditorCorrelationId });
        LOG.infof("creditorCorrelationId: \t %s", creditorCorrelationId);
        retrievedAccountMap.put(creditorCorrelationId.getId(), Uni.createFrom().nullItem());

        JsonObject cJsonObj = new JsonObject(new Gson().toJson(cEv));
        LOG.info("Sending creditor request");
        requestRetrieveAccount.send(Message.of(cJsonObj, Metadata.of(meta)));

        Uni<Account> debtorRequest = tokenRequest.onItem().transformToUni(uuid -> retrievedAccountMap.get(uuid))
                .onItem().transformToUni(account -> retrievedAccountMap.get(debtorCorrelationId.getId()))
                .onItem().invoke(c -> LOG.infof("debtor retrieved acc of:%s", c))
                // Check for failure
                .onItem().ifNull().fail()
                // Deal with Failure
                .onFailure().retry().withBackOff(Duration.ofMillis(300), Duration.ofSeconds(10)).atMost(5)
                .onItem().call(() -> retrievedAccountMap.remove(debtorCorrelationId.getId()));

        Uni<Account> creditorRequest = Uni.createFrom().item(retrievedAccountMap.get(creditorCorrelationId.getId()))
                .onItem().transformToUni(account -> retrievedAccountMap.get(creditorCorrelationId.getId()))
                .onItem().invoke(c -> LOG.infof("creditor retrieved acc of:%s", c))
                // Check for failure
                .onItem().ifNull().fail()
                // Deal with Failure
                .onFailure().retry().withBackOff(Duration.ofMillis(300), Duration.ofSeconds(10)).atMost(5)
                .onItem().call(() -> retrievedAccountMap.remove(creditorCorrelationId.getId()));

        Uni<BankService> bankAccountService = Uni.createFrom().item(new BankServiceService().getBankServicePort());

        return Uni.combine().all().unis(debtorRequest, creditorRequest, bankAccountService).asTuple()
                .onItem().ifNull().fail().log()
                .onFailure().retry().withBackOff(Duration.ofMillis(300), Duration.ofSeconds(10)).atMost(5)
                .map(tuple -> {
                    try {
                        tuple.getItem3().transferMoneyFromTo(tuple.getItem1().getBankAccountId(),
                                tuple.getItem2().getBankAccountId(), pReq.amount, pReq.description);
                    } catch (BankServiceException_Exception e) {
                        return e.getMessage();
                    }
                    // Replace BankAccountId with Token...
                    payments.add(new PaymentRecord(tuple.getItem1().getAccountUUID(), tuple.getItem2().getAccountUUID(),
                            tuple.getItem1().getBankAccountId(), pReq.amount, pReq.description));
                    return "Success";
                }).map(string -> new Event("payment-completed", new Object[] { string, corrId }))
                .map(this::convertObjectToJson);
    }

    /**
     * @author Jonas
     * @param userUUID
     * @return
     */
    List<List<String>> getCustomerReport(final UUID userUUID) {
        List<List<String>> out = new ArrayList<>();
        for (final var record : payments) {
            List<String> row = new ArrayList<>();
            if (userUUID.equals(record.debtorUUID)) {
                row.add(record.debtorToken.toString());
                row.add(record.creditorUUID.toString());
                row.add(record.amount.toString());
                row.add(record.description.toString());
            }
            out.add(row);
        }
        return out;
    }

    /**
     * @author Jonas
     * @param userUUID
     * @return
     */
    List<List<String>> getMerchantReport(final UUID userUUID) {
        List<List<String>> out = new ArrayList<>();
        for (final var record : payments) {
            List<String> row = new ArrayList<>();
            if (userUUID.equals(record.debtorUUID)) {
                row.add(record.debtorToken.toString());
                row.add(record.amount.toString());
                row.add(record.description.toString());
            }
            out.add(row);
        }
        return out;
    }

    /**
     * @author Jonas
     * @return
     */
    List<List<String>> getManagerReport() {
        List<List<String>> out = new ArrayList<>();
        for (final var record : payments) {
            List<String> row = new ArrayList<>();
            row.add(record.debtorUUID.toString());
            row.add(record.debtorToken.toString());
            row.add(record.creditorUUID.toString());
            row.add(record.amount.toString());
            row.add(record.description.toString());

            out.add(row);
        }
        return out;
    }

    private <T> T convertJsonToObject(JsonObject jObj, Class<T> classType) {
        return new Gson().fromJson(jObj.toString(), classType);
    }

    private JsonObject convertObjectToJson(Object obj) {
        return new JsonObject(new Gson().toJson(obj, obj.getClass()));
    }
    // public void paymentRequest(PaymentRequest paymentRequest) {
    // // PaymentRequest paymentRequest = event.getArgument(0,
    // PaymentRequest.class);

    // final Event tokenToUserID = new Event(
    // "tokenToUserIDRequest",
    // new Object[]{paymentRequest.debtorToken}
    // );

    // mq.publish(tokenToUserID);

    // final UUID debtorID = tokenToUserIDFuture.join();

    // final Event debtorBankIDEvent = new Event(
    // "userIDToBankIDRequest",
    // new Object[]{debtorID}
    // );

    // mq.publish(debtorBankIDEvent);

    // final String debtorBankID = userIDtoBankIDFuture.join();

    // final Event creditorBankIDEvent = new Event(
    // "userIDToBankIDRequest",
    // new Object[]{paymentRequest.creditorID}
    // );

    // mq.publish(creditorBankIDEvent);

    // final String creditorBankID = userIDtoBankIDFuture.join();
    // try{
    // bankService.transferMoneyFromTo(
    // debtorBankID,
    // creditorBankID,
    // paymentRequest.amount,
    // paymentRequest.description
    // );
    // } catch(Exception e){
    // // Publish some exception
    // return ;
    // }

    // final PaymentRecord paymentRecord = new PaymentRecord(
    // debtorBankID,
    // creditorBankID,
    // paymentRequest.debtorToken,
    // paymentRequest.amount,
    // paymentRequest.description
    // );

    // ledger.add(paymentRecord);
    // }
}