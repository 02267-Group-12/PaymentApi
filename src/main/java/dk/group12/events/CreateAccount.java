package dk.group12.events;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CreateAccount {
  
  private String accountId;
  private String bankAccountId;
  private String accountType;

}
